import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_post_count():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    options = webdriver.ChromeOptions()
    options.add_argument('ignore-certificate-errors')
    browser = Chrome(executable_path=ChromeDriverManager().install(), chrome_options=options)

    # Otwarcie strony

    # Pobranie listy tytułów

    # Asercja że lista ma 7 elementów

    # Zamknięcie przeglądarki


def test_post_count_after_search():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    options = webdriver.ChromeOptions()
    options.add_argument('ignore-certificate-errors')
    browser = Chrome(executable_path=ChromeDriverManager().install(), chrome_options=options)

    # Otwarcie strony

    # Inicjalizacja searchbara i przycisku search

    # Szukanie

    # Czekanie na stronę

    # Pobranie listy tytułów

    # Asercja że lista ma 4 elementy

    # Zamknięcie przeglądarki
    browser.quit()


# def test_post_count_on_cypress_label():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager

    # Otwarcie strony

    # Inicjalizacja elementu z labelką

    # Kliknięcie na labelkę

    # Czekanie na stronę

    # Pobranie listy tytułów

    # Asercja że lista ma 15 elementów

    # Zamknięcie przeglądarki
